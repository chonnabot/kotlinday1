data class Teacher(var name: String, var courseName: String)

fun main (args: Array<String>){
    var teacher1 : Teacher = Teacher("Somsak","Democratic")
    var teacher2 : Teacher = Teacher("Somsak","Democratic")
    var teacher3 : Teacher = Teacher("Somnuk","Mathematics")
    println(teacher1.component1())
    println(teacher2.component2())
    println(teacher3)
    val (teacherName, teacherCourseName) = teacher3
    println("$teacherName teaches $teacherCourseName")
}