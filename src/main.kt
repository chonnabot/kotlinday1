abstract class Person(var name: String, var surname: String, var gpa: Double) {
    constructor(name: String, surname: String) : this(name, surname, 0.0) {

    }
    abstract fun goodBoy():Boolean
    open fun getDetils(): String {
        return "$name $surname has score $gpa"
    }
}


class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
    constructor(name: String, surname: String, department: String) : this(name, surname, 0.0, department) {

    }
    override fun goodBoy(): Boolean {
      return gpa > 2.0
    }
    override fun getDetils(): String {
        return "$name $surname has score $gpa and study in $department"
    }

}

fun main(args: Array<String>) {
    val no1: Student = Student("Sonchai1", "Pitak1", "CAMT")
    val no2: Student = Student("Sonchai2", "Pitak2",2.00 ,"CAMT")
    val no3: Student = Student("Sonchai3", "Pitak3", 3.00 ,"CAMT")

    println(no1.getDetils())
    println(no2.getDetils())
    println(no3.getDetils())
}