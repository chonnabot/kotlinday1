abstract class Animal(var noOfLegs: Int, var food: String) {
    abstract fun soundAnimal() : String
    open fun getAnimal(): String {
        return "$noOfLegs eat $food"
    }
}

class Dog(noOfLegs: Int, food: String, var name : String) : Animal(noOfLegs, food) {
    override fun soundAnimal() : String{
        return "$name have " + super.getAnimal() + " Sound Bak Bok!!"
    }
}

class Lion(noOfLegs: Int, food: String) : Animal(noOfLegs, food) {
    override fun soundAnimal() : String{
        return "Lion have " + super.getAnimal() + " Sound Ark Ark!!"
    }
}


fun main(args: Array<String>) {
    val no1:Dog = Dog(4,"meat","Dog")
    val no2:Lion = Lion(4,"meat")

    println(no1.soundAnimal())
    println(no2.soundAnimal())
}